import React, { Component } from 'react';
import Header from './Header/Header';
import Body from './Body/Body';

export default function App(){
    return(
        <div>
            <Header />
            <Body />
        </div>
    )
}