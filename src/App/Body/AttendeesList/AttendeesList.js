import React from 'react';
import './AttendeesList.css';

export default function AttendeesList(props){
    function handleRemove(id){
        props.onAttendeeRemove(id);
    }
    return(
        <div className="attendees">
            {props.attendeeList.map(attendee =>
                <div className='attendee' key={attendee.id}>
                    <div className="attendeeCard">
                        <header style={{"backgroundColor": attendee.favouriteColor}}>
                            <h2>Hello</h2>
                            <h4>My name is</h4>
                        </header>
                        <main>
                            {attendee.attendeeName}
                        </main>
                        <footer style={{"backgroundColor": attendee.favouriteColor}}>
                        </footer>
                    </div>
                    <div className='removeBtn'>
                        <button onClick={() => handleRemove(attendee.id)}>x Remove attendee</button>
                    </div>
                </div>
            )}
        </div>
    )
}