import React from 'react';
import AttendeesList from './AttendeesList/AttendeesList';
import InputSection from './InputSection/InputSection';

export default class Body extends React.Component{

    constructor(props){
        super();
        this.state = {
            attendeeList: [],
            uniqueId: 0
        }
        this.removeAttendee = this.removeAttendee.bind(this);
        this.addAttendee = this.addAttendee.bind(this);
    }
    removeAttendee(id){
        this.setState((prevState, props) => ({
            attendeeList: prevState.attendeeList.filter(attendee => attendee.id !== id)
        }))
    }
    addAttendee(attendeeName, favouriteColor){
        this.setState((prevState, props) => ({
            attendeeName: prevState.attendeeList.push({id: prevState.uniqueId, attendeeName: attendeeName, favouriteColor: favouriteColor}),
            uniqueId: prevState.uniqueId+1
        }))
    }
    render(){
        return(
            <div class="body">
                <InputSection onAttendeeAdd={this.addAttendee}/>
                <AttendeesList attendeeList={this.state.attendeeList} onAttendeeRemove={this.removeAttendee}/>
            </div>
        )
    }
}