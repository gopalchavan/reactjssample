import React from 'react';

export default class InputSection extends React.Component{
    constructor(props){
        super();
        this.state = {
            attendeeName: '',
            favouriteColor: ''
        }
        this.handleAttendeeNameChange = this.handleAttendeeNameChange.bind(this);
        this.handleFavouriteColorChange = this.handleFavouriteColorChange.bind(this);
        this.addAttendee = this.addAttendee.bind(this);
    }

    handleAttendeeNameChange(e){
        this.setState({
            attendeeName: e.target.value
        })
    }

    handleFavouriteColorChange(e){
        this.setState({
            favouriteColor: e.target.value
        })
    }
    addAttendee(){
        this.props.onAttendeeAdd(this.state.attendeeName,this.state.favouriteColor);
        this.setState({
            attendeeName: '',
            favouriteColor: ''
        })
    }

    render(){
        return(
            <div>
                <label>Name</label>
                <input type='text' value={this.state.attendeeName} onChange={this.handleAttendeeNameChange} placeholder='John Doe'/>
                <label>Favourite Color</label>
                <input type='text' value={this.state.favouriteColor} onChange={this.handleFavouriteColorChange} placeholder='#fefefe'/>
                <button onClick={this.addAttendee}>Add attendee</button>
            </div>
        )
    }
}